package yami.birthdays;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Taingus on 31/07/2016.
 */
public class Preferences {
    public static final String PREFERENCES_NAME = "com.cme.birthdays.PREFERENCES";
    private static final String LIST_CHANGED ="com.cme.birthdays.LIST_CHANGED";
    private static Preferences instance;
    private static SharedPreferences preferences;
    private static SharedPreferences.Editor editor;


    private Preferences(Context context) {
        preferences = context.getSharedPreferences(PREFERENCES_NAME, 0);
        editor = preferences.edit();
    }

    public static Preferences getInstance(Context context) {
        if (instance == null) {
            instance = new Preferences(context);
        }
        return instance;
    }

    public boolean getListChanged(){
        return preferences.getBoolean(LIST_CHANGED, false);
    }

    public void setListChanged(boolean newValue){
        editor.putBoolean(LIST_CHANGED, newValue);
        editor.commit();
    }
}
