package yami.birthdays;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import yami.birthdays.Item.Item;
import yami.birthdays.Item.ItemAdapter;
import yami.birthdays.Item.ItemFactory;

public class MainActivity extends AppCompatActivity {
    private ListView itemList;
    private ItemAdapter itemAdapter;
    private ItemFactory itemFactory;
    private Preferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        itemList = (ListView) findViewById(R.id.list);
        itemFactory = ItemFactory.getInstance(this);
        preferences= Preferences.getInstance(getApplicationContext());
        try {
            itemAdapter = new ItemAdapter(itemFactory.getItemList());
            itemList.setAdapter(itemAdapter);
            itemList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Item selected = (Item) parent.getItemAtPosition(position);
                    Intent description = new Intent(getApplicationContext(), ContactDetail.class);
                    description.putExtra("item", selected);
                    description.putExtra("position", position);
                    startActivity(description);
                }
            });
        } catch (Exception e) {
            Log.e("MainActivity:onCreate", e.getMessage());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent create;
        switch(item.getItemId()) {
            case R.id.add_item:
                create = new Intent(this, AddContact.class);
                startActivity(create);
                return true;
            case R.id.about:
                create = new Intent(this, About.class);
                startActivity(create);
                return true;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        try {
            if(preferences.getListChanged()) {
                itemAdapter.setNewElements(itemFactory.getItemList());
                itemAdapter.notifyDataSetChanged();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    public void nextsBirthdays(View v) throws SQLException {
        java.util.Date current_date = new java.util.Date();
        current_date.setYear(0);
        current_date.setHours(0);
        current_date.setMinutes(0);
        current_date.setSeconds(0);
        try {
            List<Item> result= ItemFactory.getInstance(this).nextsBirthdays(current_date);
            itemAdapter.setNewElements(result);
            itemAdapter.notifyDataSetChanged();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    public void orderByName(View v) throws SQLException {
        try {
            List<Item> result= ItemFactory.getInstance(this).orderByName();
            itemAdapter.setNewElements(result);
            itemAdapter.notifyDataSetChanged();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

    public void getAll(View v) throws SQLException {
        try {
            List<Item> result= ItemFactory.getInstance(this).getItemList();
            itemAdapter.setNewElements(result);
            itemAdapter.notifyDataSetChanged();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        super.onResume();
    }

}
