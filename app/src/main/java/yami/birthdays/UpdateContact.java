package yami.birthdays;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;

import yami.birthdays.Item.Item;
import yami.birthdays.Item.ItemFactory;

public class UpdateContact extends AppCompatActivity {
    private DatePicker date_picker;
    private EditText contact_name;
    private Item showing;
    private Preferences preferences;
    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_contact);
        showing = (Item) getIntent().getSerializableExtra("item");
        date_picker = (DatePicker) findViewById(R.id.contact_date_born);
        contact_name = (EditText) findViewById(R.id.contact_name);
        preferences= Preferences.getInstance(getApplicationContext());
        Date current = new Date();
        Date bornDate=showing.getBorn();
        int year;

        date_picker.setMaxDate(current.getTime());//set the current day as the max date
        contact_name.setText(showing.getName());
        year = bornDate.getYear() + 1900;
        date_picker.updateDate(year, bornDate.getMonth(), bornDate.getDate());
    }

    public boolean updateContact(View v) {
        String name = contact_name.getText().toString();
        if(0 == name.length()) {
            Toast.makeText(getApplicationContext(), R.string.name_required,Toast.LENGTH_LONG).show();
            return false;
        } else {
            int year = date_picker.getYear() - 1900;
            showing.setName(name);
            showing.setBorn(new Date(year, date_picker.getMonth(), date_picker.getDayOfMonth()));
            showing.setBirthday(new Date(0, date_picker.getMonth(), date_picker.getDayOfMonth()));
            ItemFactory.getInstance(this).updateItem(showing);
            preferences.setListChanged(true);
            onBackPressed();
            return true;
        }
    }
}
