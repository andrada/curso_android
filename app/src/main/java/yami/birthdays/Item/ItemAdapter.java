package yami.birthdays.Item;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import yami.birthdays.R;

public class ItemAdapter extends BaseAdapter {

    private List<Item> items;

    public ItemAdapter(List<Item> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Item getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return items.get(position).getMyId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View currentView;

        if (convertView == null) {
            currentView = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_item_adapter, parent, false);
        } else {
            currentView = convertView;
        }

        Item item = getItem(position);

        TextView description = (TextView) currentView.findViewById(R.id.view_contact_name);
        TextView birth = (TextView) currentView.findViewById(R.id.view_birthday);
        Date birthday = item.getBirthday();
        Date today = new Date();
        int diffMonth = today.getMonth() - birthday.getMonth();
        int diffDay = today.getDate() - birthday.getDate();

        description.setText(item.getName());
        if(diffMonth==0 && diffDay==0) {
            birth.setText(R.string.cumple_hoy);
        }else {
            birth.setText("");
        }
        return currentView;
    }
    public void setNewElements(List<Item> newElements) {
        items = newElements;
    }
}