package yami.birthdays.Item;

import android.renderscript.Element;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;


import java.io.Serializable;
import java.util.Date;

/**
 * Created by Taingus on 27/07/2016.
 */
public class Item implements Serializable {

    @DatabaseField(generatedId = true, allowGeneratedIdInsert = true)
    private long  myId;

    @DatabaseField
    private String  name;

    @DatabaseField
    private Date born;

    @DatabaseField
    private Date birthday;

    @DatabaseField
    private int day;

    @DatabaseField
    private int month;

    public Item() {}

    public Item(String name, Date born,Date birthday,int day, int month) {
        myId = 0;
        this.name = name;
        this.born = born;
        this.birthday = birthday;
        this.day=day;
        this.month=month;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getMyId() {
        return myId;
    }

    public Date getBorn() {
        return born;
    }

    public void setBorn(Date born) {
        this.born = born;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
}
