package yami.birthdays.Item;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import yami.birthdays.database.DatabaseHelper;

/**
 * Created by Taingus on 27/07/2016.
 */
public class ItemFactory {
    private static ItemFactory instance;
    private Dao<Item, Integer> itemDao;

    public ItemFactory(Context context){
        OrmLiteSqliteOpenHelper helper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        try {
            itemDao = helper.getDao(Item.class);
        } catch (SQLException e) {
            // Boo hoo
        }
    }

    public static ItemFactory getInstance(Context context) {
        if (instance == null) {
            instance = new ItemFactory(context);
        }
        return instance;
    }

    public static void setInstance(ItemFactory instance) {
        ItemFactory.instance = instance;
    }

    public List<Item> getItemList() throws SQLException {
        return itemDao.queryForAll();
    }

    public boolean addItem(Item newItem) {
        try {
            itemDao.create(newItem);
        } catch (SQLException e) {
            return false;
        }

        return true;
    }

    public boolean removeItem(Item toDelete) {
        try {
            itemDao.delete(toDelete);
        } catch (SQLException e) {
            return false;
        }

        return true;
    }
    public boolean updateItem(Item data) {
        try {
            itemDao.update(data);
        } catch (SQLException e) {
            return false;
        }
        return true;
    }
    public List<Item> nextsBirthdays(Date date)throws SQLException {
        //List<Item> res=itemDao.query(itemDao.queryBuilder().limit(10).where().gt("birthday", date).prepare());
        //List<Item> results=itemDao.queryBuilder().orderBy("birthday",true).limit(10).where().ge("birthday",date).or().eq("month",date.getMonth()).and().eq("day",date.getDate()).query();

        QueryBuilder<Item, Integer> xQB = itemDao.queryBuilder().orderBy("birthday",true).limit(10);
        Where<Item, Integer> where = xQB.where();
        where.or(where.ge("birthday",date),where.and(where.eq("month",date.getMonth()),where.eq("day",date.getDate())));
        List<Item> results=where.query();

        // Iterator<Item> iterator = itemDao.queryBuilder().limit(10).where().gt("birthday", date).iterator();
        //Log.e("MainActivity:onCreate", String.valueOf(results.getResults().size()));
        if(results.size() < 10){
            int limit= 10 - results.size();
            QueryBuilder<Item, Integer> xQB2 = itemDao.queryBuilder().orderBy("birthday",true).limit(limit);
            Where<Item, Integer> where2 = xQB2.where();
            where2.or(where2.lt("month",date.getMonth()),where2.and(where2.eq("month",date.getMonth()),where2.lt("day",date.getDate())));
            List<Item> nextYear=where2.query();
            if(nextYear.size()>0) {
                results.addAll(nextYear);
            }
        }
        return results;
    }

    public List<Item>  orderByName() throws SQLException{
        List<Item> results;
        results = itemDao.queryBuilder().orderBy("name",true).query();
        return results;
    }
}
