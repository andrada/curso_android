package yami.birthdays;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;

import com.j256.ormlite.field.types.DateType;
import com.j256.ormlite.field.types.SqlDateType;

import java.util.Date;

import yami.birthdays.Item.Item;
import yami.birthdays.Item.ItemFactory;

public class AddContact extends AppCompatActivity {
    private DatePicker date_picker;
    private CalendarView calendar_view;
    private EditText contact_name;
    private Preferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);
        date_picker = (DatePicker) findViewById(R.id.contact_date_born);
        calendar_view = date_picker.getCalendarView();
        contact_name = (EditText) findViewById(R.id.contact_name);
        preferences= Preferences.getInstance(getApplicationContext());
        Date current = new Date();
        date_picker.setMaxDate(current.getTime());//set the current day as the max date
        /* Add  listener *****************************************
        if (calendar_view != null) {
            //OnDateSetListener  or OnDateChangedListener
            /*datePicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    Toast.makeText(getApplicationContext(), "test" + hasFocus,Toast.LENGTH_LONG).show();
                }
            });//
            calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                @Override
                public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                    Toast.makeText(getApplicationContext(),dayOfMonth+ "-"+month+"-"+year,Toast.LENGTH_LONG).show();
                    current_date=dayOfMonth+ "-"+month+"-"+year;
                }
            });
            datePicker.updateDate(1989,10,01);
            first_date= datePicker.getDayOfMonth()+ "-"+datePicker.getMonth()+"-"+datePicker.getYear();

            Toast.makeText(getApplicationContext(),first_date,Toast.LENGTH_LONG).show();
        }*/
    }
    public boolean addElement(View v) {
        String name = contact_name.getText().toString();
        if(name.length()==0) {
            Toast.makeText(this, R.string.name_required,Toast.LENGTH_LONG).show();
            return false;
        } else {
            int year = date_picker.getYear() - 1900;
            Item newItem= new Item(name,
                    new Date(year, date_picker.getMonth(),date_picker.getDayOfMonth()),
                    new Date(0,date_picker.getMonth(),date_picker.getDayOfMonth()),
                    date_picker.getDayOfMonth(),
                    date_picker.getMonth());
            ItemFactory.getInstance(this).addItem(newItem);
            preferences.setListChanged(true);
            onBackPressed();
            return true;
        }
    }
}
