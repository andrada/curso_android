package yami.birthdays;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import yami.birthdays.Item.Item;
import yami.birthdays.Item.ItemFactory;

public class ContactDetail extends AppCompatActivity {
    private Item showing;
    private Preferences preferences;
    private boolean itsBirthday;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_detail);
        showing = (Item) getIntent().getSerializableExtra("item");
        Date dateBorn = showing.getBorn();
        Date birthday = showing.getBirthday();
        preferences= Preferences.getInstance(getApplicationContext());
        SimpleDateFormat dt = new SimpleDateFormat("EEE, d MMM yyyy");
        SimpleDateFormat dtB = new SimpleDateFormat("dd/MM/yyyy");
        String  my_birthday;
        itsBirthday=false;


        Date today = new Date();
        int diffYear = today.getYear() - dateBorn.getYear();
        int diffMonth = today.getMonth() - dateBorn.getMonth();
        int diffDay = today.getDate() - dateBorn.getDate();
        // Si está en ese año pero todavía no los ha cumplido
        if (diffMonth < 0 || (diffMonth == 0 && diffDay < 0)) {
            diffYear=diffYear-1;
        }
        birthday.setYear(today.getYear());
        if(diffMonth >0 || (diffMonth==0 && diffDay> 0)) {
            birthday.setYear(birthday.getYear()+1);
        }
        my_birthday = dt.format(birthday);

        ((TextView) findViewById(R.id.detail_contact_name)).setText(showing.getName());
        ((TextView) findViewById(R.id.detail_contact_date_born)).setText(dtB.format(dateBorn));
        ((TextView) findViewById(R.id.detail_contact_date_birthday)).setText(my_birthday);
        ((TextView) findViewById(R.id.detail_contact_age)).setText(String.valueOf(diffYear));
        preferences.setListChanged(false);
        if(diffMonth==0 && diffDay==0) {
            LinearLayout layout= ((LinearLayout) findViewById(R.id.DinamicMessage));
            TextView current = new TextView(this);
            itsBirthday=true;
            current.setText(R.string.cumple_hoy);
            if (Build.VERSION.SDK_INT < 23) {
                current.setTextAppearance(getApplicationContext(), R.style.Message);
            } else{
                current.setTextAppearance(R.style.Message);
            }
            layout.addView(current);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.delete_contact:
                ItemFactory.getInstance(this).removeItem(showing);
                preferences.setListChanged(true);
                onBackPressed();
                return true;
            case R.id.edit_contact:
                Intent update = new Intent(this, UpdateContact.class);
                update.putExtra("item", showing);
                startActivity(update);
                finish();
                return true;
            case R.id.Share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                if(itsBirthday) {
                    sendIntent.putExtra(Intent.EXTRA_TEXT,getResources().getString(R.string.message_birthday));
                    sendIntent.setType("text/plain");
                    /*Resources resources = getApplicationContext().getResources();
                    sendIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://"
                            + resources.getResourcePackageName(R.drawable.cupcake)
                            + '/' + resources.getResourceTypeName(R.drawable.cupcake)
                            + '/' + resources.getResourceEntryName(R.drawable.cupcake)
                    +".jpg"));
                    sendIntent.setType("image/jpeg");*/
                }
                startActivity(sendIntent);
                return true;
            default: break;
        }
        return super.onOptionsItemSelected(item);
    }
}
